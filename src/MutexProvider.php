<?php declare(strict_types=1);

namespace Adduc\LaravelMutex;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\ServiceProvider;

class MutexProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Mutex::class, function () {
            /** @todo use config to determine mutex to resolve */
            return $this->app->make(Database::class);
        });

        $this->app->singleton(Database::class, function () {
            $connection = $this->app->make(ConnectionInterface::class);
            return new Database($connection);
        });
    }
}
