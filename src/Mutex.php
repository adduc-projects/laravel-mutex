<?php declare(strict_types=1);

namespace Adduc\LaravelMutex;

interface Mutex
{
    public function lock(string $key, int $timeout_ms = 0): bool;
}
