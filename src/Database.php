<?php declare(strict_types=1);

namespace Adduc\LaravelMutex;

use Illuminate\Database\ConnectionInterface;

class Database implements Mutex
{
    /**
     * @var ConnectionInterface
     */
    protected $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function lock(string $key, int $timeout_ms = 0): bool
    {
        $sql = 'SELECT GET_LOCK(:key, :timeout_sec) as `lock`';
        $data = ['key' => $key, 'timeout_sec' => (float)($timeout_ms / 1000)];

        $result = $this->connection->selectOne($sql, $data);
        return (bool)$result->lock;
    }
}
